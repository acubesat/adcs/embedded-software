#ifndef ADCS_SOFTWARE_UPDATEPARAMETERSTASK_HPP
#define ADCS_SOFTWARE_UPDATEPARAMETERSTASK_HPP

#include "Task.hpp"

/**
 * FreeRTOS task for periodically updating specific parameters using ParameterService functionality.
 */
class UpdateParametersTask : public Task {
private:
    const uint16_t delayMs = 3000;
public:
    const static inline uint16_t TaskStackDepth = 9000;

    StackType_t taskStack[TaskStackDepth];

    void execute();

    UpdateParametersTask() : Task("ParameterUpdating") {}

    /**
     * Create FreeRTOS task
     */
    void createTask() {
        xTaskCreateStatic(vClassTask < UpdateParametersTask > , this->TaskName, UpdateParametersTask::TaskStackDepth,
                          this, tskIDLE_PRIORITY, this->taskStack, &(this->taskBuffer));
    }

};

inline std::optional<UpdateParametersTask> updateParametersTask;

#endif